package com.academiamoviles.sesion01app

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_generaciones.*

class GeneracionesActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generaciones)

        btnVerificar.setOnClickListener {

            val anio = edtAnio.text.toString().toInt()

            val generacion = when (anio) {
                in 1930..1948 -> {
                    "SILENT GENERATION"
                }
                in 1949..1968 -> {
                    "BABY BOOM"
                }
                in 1969..1980 -> {
                    "GENERACION X"
                }
                in 1981..1993 -> {
                    "GENERACION Y"
                }
                in 1994..2010 -> {
                    "GENERACION Z"
                }
                else -> {
                    "NO DEFINIDO"
                }
            }
            
            tvResultadoGeneracion.setText("Generación:  ${generacion}")

            val poblacion = when (anio) {
                in 1930..1948 -> {
                    "6,300.000"
                }
                in 1949..1968 -> {
                    "12,200.000"
                }
                in 1969..1980 -> {
                    "9,300.000"
                }
                in 1981..1993 -> {
                    "7,200.000"
                }
                in 1994..2010 -> {
                    "7,8000.000"
                }
                else -> {
                    "NO DEFINIDO"
                }
            }

            tvResultadoPoblacion.setText ("Población: ${poblacion}")

            val  rasgos  = when (anio) {
                in 1930..1948 -> {
                    R.drawable.austeridad
                }
                in 1949..1968 -> {
                    R.drawable.ambicion
                }
                in 1969..1980 -> {
                    R.drawable.obsesion
                }
                in 1981..1993 -> {
                    R.drawable.frustracion
                }
                in 1994..2010 -> {
                    R.drawable.irreverencia
                }
                else -> {
                    R.drawable.ic_launcher_foreground
                }
            }

            ivRasgos.setImageResource(rasgos)
        }
    }
}